#!/bin/bash
#
#
# --- define instance name ---
vm_name="delphi-test"

iteration=$(openstack server list | grep ${vm_name} | wc -l)
# remove any whitespace from iteration
iteration=$(echo -e "${iteration}" | tr -d '[:space:]')
vm_name="${vm_name}${iteration}"

# --- swap in your ssh key ---
sshkey=`cat ~/.ssh/id_rsa.pub`

context_file="/tmp/${USER}/context-${vm_name}-$(date +%Y%m%d%H%M%S).yaml"
sed 's:\[your ssh public key\]:${sshkey}:g' < cernvm_context.yaml > "${context_file}"

# --- launch instance ---
echo Launching ${vm_name}
openstack server create --image "3bd60cc0-0e2a-4489-95c6-4a4fe49f8b63" --flavor m2.medium --user-data "${context_file}" ${vm_name}
