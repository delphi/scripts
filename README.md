# DELPHI VM Cofiguration Scripts

The `cernvm_context.yaml` is cloud-init user-data to instantiate a SLC6 or CernVM3 instance on a cloud infrastructure supporting user-data (such as OpenStack or Amazon AWS). Specifically for CernVM you may pin the operating system to a tested SL6 release with the use of `ucernvm.config`. To do this use the `mime-combiner.py` utility to combine the cloud-inti file with the cernvm configuration file like this:

    ./mime-combiner.py ucernvm.config:text/ucernvm-config cernvm_context.yaml:text/cloud-config >> user-data.txt

Use the generated user-data.txt to instantiate you CernVM instance.
